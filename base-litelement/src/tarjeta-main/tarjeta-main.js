import { LitElement, html } from 'lit-element';  
import '../tarjeta-ficha-listado/tarjeta-ficha-listado.js';
import '../tarjeta-nueva/tarjeta-nueva.js';
import '../tarjeta-detalle/tarjeta-detalle.js';
import '../tarjeta-recarga/tarjeta-recarga.js';


class TarjetaMain extends LitElement {   

    static get properties(){
		return {
            cliente: {type:Object},
            tarjetas: {type: Array},
            mostrarFormTarjeta: {type: Boolean},
            mostrarFormDetalleTarjeta: {type: Boolean},
            mostrarFormRecargaTarjeta: {type: Boolean},
		};
    }
    
    constructor(){
		super();
		this.tarjetas=[];
        this.getTarjetasData();
        this.mostrarFormTarjeta = false;
        this.mostrarFormDetalleTarjeta = false;
        this.mostrarFormRecargaTarjeta = false;
		
	}

    render() {
        return html `
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            

        
            <!-- <h2 class="text-center">Lista Tarjetas</h2> -->
            <main>
             <div class="row" id="listaTarjetas">
                <div class="row row-cols-1 row-cols-sm-3">
                   ${this.tarjetas.map(
                    tarjeta => html `<tarjeta-ficha-listado 
                    numero="${tarjeta.cliente}" cliente="${tarjeta.tarjetas[0].numero_tarjeta}" 
                    saldo="${tarjeta.tarjetas[0].saldo_inicial}" categoria="${tarjeta.tarjetas[0].categoria}"
                    @ver-tarjeta="${this.verTarjeta}"
                    @recargar-tarjeta="${this.recargarTarjeta}"
                    >
                    </tarjeta-ficha-listado>`
                 )}
                </div>
             </div>
             <div class="row">
                    <tarjeta-nueva id="formTarjeta" class="d-none border rounded border-primary" @tarjeta-cerrar-form="${this.cerrarFormTarjeta}"></tarjeta-nueva>
                    <tarjeta-detalle id="formDetalleTarjeta" class="d-none border rounded border-primary" @tarjeta-detalle-cerrar-form="${this.cerrarFormDetalleTarjeta}"></tarjeta-detalle>
                    <tarjeta-recarga id="formRecargaTarjeta" class="d-none border rounded border-primary" @tarjeta-recarga-cerrar-form="${this.cerrarFormRecargaTarjeta}"></tarjeta-recarga>
             </div>
            </main>

        `;
    }

    updated(changedProperties) {
        console.log("updated");
        if (changedProperties.has("mostrarFormTarjeta")) {
            console.log("Ha cambiado el valor de mostrarFormTarjeta en tarjeta-main");
            if (this.mostrarFormTarjeta === true) {
                this.mostrarFormTarjetaData();
            } else {
                this.mostrarListaTarjeta();
            }
        }

        if (changedProperties.has("mostrarFormDetalleTarjeta")) {
            console.log("Ha cambiado el valor de mostrarDetalleTarjeta en tarjeta-main");
            if (this.mostrarFormDetalleTarjeta === true) {
                this.mostrarDetalleFormTarjeta();
            } else {
                this.mostrarListaTarjeta();
            }
        }

        if (changedProperties.has("mostrarFormRecargaTarjeta")) {
            console.log("Ha cambiado el valor de mostrarDetalleTarjeta en tarjeta-main");
            if (this.mostrarFormRecargaTarjeta === true) {
                this.mostrarRecargaFormTarjeta();
            } else {
                this.mostrarListaTarjeta();
            }
        }

    }

    mostrarListaTarjeta() {
        console.log("mostrarListaTarjeta");
        console.log("Mostrando listado de tarjetas");
        this.shadowRoot.getElementById("listaTarjetas").classList.remove("d-none");
        this.shadowRoot.getElementById("formTarjeta").classList.add("d-none");
        this.shadowRoot.getElementById("formDetalleTarjeta").classList.add("d-none");
        this.shadowRoot.getElementById("formRecargaTarjeta").classList.add("d-none");
    }

    mostrarFormTarjetaData() {
        console.log("mostrarFormTarjetaData");
        console.log("Mostrando formulario de tarjeta");
        this.shadowRoot.getElementById("listaTarjetas").classList.add("d-none");
        this.shadowRoot.getElementById("formTarjeta").classList.remove("d-none");
    }

    mostrarDetalleFormTarjeta() {
        console.log("mostrarDetalleFormTarjeta");
        console.log("Mostrando formulario de Detalle de tarjeta");
        this.shadowRoot.getElementById("listaTarjetas").classList.add("d-none");
        this.shadowRoot.getElementById("formDetalleTarjeta").classList.remove("d-none");
    }

    mostrarRecargaFormTarjeta() {
        console.log("mostrarRecargaFormTarjeta");
        console.log("Mostrando formulario de recarga de tarjeta");
        this.shadowRoot.getElementById("listaTarjetas").classList.add("d-none");
        this.shadowRoot.getElementById("formRecargaTarjeta").classList.remove("d-none");
    }

    cerrarFormTarjeta() {
        console.log("cerrarFormTarjeta");
        console.log("Se ha cerrado el formulario de tarjeta");
        this.mostrarFormTarjeta = false;
    }

    cerrarFormDetalleTarjeta() {
        console.log("cerrarFormDetalleTarjeta");
        console.log("Se ha cerrado el formulario de detalla de tarjeta");
        this.mostrarFormDetalleTarjeta = false;
    }

    cerrarFormRecargaTarjeta() {
        console.log("cerrarFormRecargaTarjeta");
        console.log("Se ha cerrado el formulario de recarga de tarjeta");
        this.mostrarFormRecargaTarjeta = false;
    }

    verTarjeta(e) {
        console.log("verTarjeta en tarjeta-main");
        console.log("Se ha solicitado más información de la tarjeta " + e.detail.id);

        this.getClientePorId(e.detail.id);
        console.log("cliente: "+this.cliente);
    
        this.shadowRoot.getElementById("formDetalleTarjeta").cliente = this.cliente;
        this.mostrarFormDetalleTarjeta = true;
    }

    recargarTarjeta(e) {
        console.log("recargarTarjeta en tarjeta-main");
        console.log("Se ha solicitado recarga de la tarjeta " + e.detail.id);
        this.mostrarFormRecargaTarjeta = true;
    }

    getTarjetasData(){
	
        let xhr = new XMLHttpRequest();
    
        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                this.tarjetas = APIResponse;
            }
        }
		
		xhr.open("GET","https://hackaton-g8.herokuapp.com/hackaton/v2/clientes");
		
		xhr.send();
		
		
    }
    
    
    getClientePorId(id){
			
        let xhr = new XMLHttpRequest();
    
        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                this.cliente = APIResponse;
            }
        }
		
		xhr.open("GET","https://hackaton-g8.herokuapp.com/hackaton/v2/clientes/"+id);
		
		xhr.send();
		
		
	}

}  
customElements.define('tarjeta-main', TarjetaMain) 
