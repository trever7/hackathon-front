import {LitElement, html} from 'lit-element';

/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class TarjetaRecarga extends LitElement {
    static get properties() {
        return {
            nroTarjeta:{type:String}, 
            categoria:{type:String}, 
            saldoTarjeta:{type:Number},
            montoRecarga:{type:Number},
        }
    }

    /**
     * Instance of the element is created/upgraded. Use: initializing state,
     * set up event listeners, create shadow dom.
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
         <form class="row align-items-start">
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col col-md-6 col-sm-3 col-xs-12">
                <div class="d-flex justify-content-center">
                     <h3>Recarga de Tarjeta</h3>
                </div>
                <div class="form-group">
                    <label>Número de Tarjeta</label>
                    <input type="text" id="fname" name="fname" value="${this.nroTarjeta}" class="form-control"
                    @input="${this.updateNrotarjeta}"></input>
                </div> 
                <div class="form-group">
                    <label>Categoría</label>
                    <select id="categoria" class="form-control">
                       <option> Transporte</option>
                       <option> GiftCard</option>
                       <option> Alimentación</option>
                    </select>
                </div>
                <div class="form-group">
                   <label>Saldo Actual</label>
                   <input type="text" name="sactual" value="${this.saldoTarjeta}" 
                   @input="${this.updateSaldoActual}" class="form-control"></input>
                </div>
                <div class="form-group">  
                    <label>Monto a Recargar</label>
                    <input type="text" name="montoreca" value="${this.montoRecarga}" class="form-control"></input>
                </div>

                <div class="card-footer">
                   <button @click="${this.regresa}" class="btn btn-danger col-5"><strong>Volver</strong></button>
                   <button @click="${this.actualizacarga}" class="btn btn-info col-5 offset-1"><strong>Recargar</strong></button>
                </div>
            </div>
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
         </form>
        `;
    }

    regresa(e) {
        console.log("regresa");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("tarjeta-recarga-cerrar-form",{}));
    }

    resetFormData() {
        console.log("resetFormData en tarjeta-nueva");
        // this.person = {};
        // this.person.name = "";
        // this.person.profile = "";
        // this.person.yearsInCompany = "";

        // this.editingPerson = false;
    }

}

customElements.define('tarjeta-recarga', TarjetaRecarga);