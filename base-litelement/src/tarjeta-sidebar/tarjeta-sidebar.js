import { LitElement, html } from 'lit-element';  
class TarjetaSideBar extends LitElement {   
  render() { 
    return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

     <aside>
	   <section>
		 <div>
         <button @click="${this.nuevaTarjeta}" class="w-100 btn bg-success" style="color:white;"><strong>Añadir Tarjeta</strong></button>
         </div>
       </section>
     </aside> 
  `;
  }
  
  nuevaTarjeta(e) {
    console.log("nuevaTarjeta en tarjeta-sidebar");
    console.log("Se va a crear un nuevo registro de tarjeta");
    this.dispatchEvent(new CustomEvent("nueva-tarjeta", {}));
  }
  
}  
customElements.define('tarjeta-sidebar', TarjetaSideBar) 