import { LitElement, html } from 'lit-element';  
import '../tarjeta-main/tarjeta-main.js';
import '../tarjeta-sidebar/tarjeta-sidebar.js';
import '../tarjeta-header/tarjeta-header.js';

class TarjetaApp extends LitElement {   
  render() { 
    return html`

      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <tarjeta-header></tarjeta-header>
      <div class="row">
		        <tarjeta-sidebar @nueva-tarjeta="${this.nuevaTarjeta}" class="col-2"></tarjeta-sidebar>
		        <tarjeta-main class="col-10"></tarjeta-main>
	  </div>
  `;
  }
  
  nuevaTarjeta(e) {
    console.log("nuevaTarjeta en TarjetaApp");
    this.shadowRoot.querySelector("tarjeta-main").mostrarFormTarjeta = true;
    console.log("Termina nuevaTarjeta");
}
  
}  
customElements.define('tarjeta-app', TarjetaApp) 