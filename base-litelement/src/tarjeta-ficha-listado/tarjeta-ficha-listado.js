import { LitElement, html } from 'lit-element';  
class TarjetaFichaListado extends LitElement {  
  
  static get properties(){
		return {
       id:{type: String},     
      numero: {type: String},
      cliente: {type: String},
      categoria: {type: String},
      saldo: {type: String},
      imagen: {type: String},
		}
	}
	
	constructor(){
    super();
    this.imagen = "./img/disfruta.jpg";
	}

  render() {
    return html `
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
       <div class="card h-100">  
          <img src="${this.imagen}" height="150" width="100" class="card-img-top"/>
          <div class="card-body">
          <h5 class="card-title">${this.cliente}</h5>
          <p class="card-text">${this.numero}</p>
          </div>  

                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        ${this.saldo} Soles de Saldo |       ${this.categoria}
                    </li>
                </ul>

          <div class="card-footer">
                    <button @click="${this.recargarTarjeta}" class="btn btn-success col-4"><strong>Recargar</strong></button>
                    <button @click="${this.verTarjeta}" class="btn btn-info col-3"><strong>Ver</strong></button>
                    <button @click="${this.borrarTarjeta}" class="btn btn-danger col-4"><strong>Borrar</strong></button>
          </div>
        </div> 

  
    `;
  }

  recargarTarjeta(e) {
    console.log("recargarTarjeta en tarjeta-ficha-listado");
   
    this.dispatchEvent(
        new CustomEvent ("recargar-tarjeta", {
                detail: {
                    id: this.id
                }
            }
        )
    );
  }

  verTarjeta(e) {
    console.log("verTarjeta en tarjeta-ficha-listado");
   
    this.dispatchEvent(
        new CustomEvent ("ver-tarjeta", {
                detail: {
                    id: this.id
                }
            }
        )
    );
  }

  borrarTarjeta(e) {
    console.log("borrarTarjeta en tarjeta-ficha-listado");
   
    this.dispatchEvent(
        new CustomEvent ("borrar-tarjeta", {
                detail: {
                    id: this.id
                }
            }
        )
    );
  }

}  
customElements.define('tarjeta-ficha-listado', TarjetaFichaListado) 