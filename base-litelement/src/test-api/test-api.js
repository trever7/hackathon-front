import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {

    static get properties() {
        return {
            movies: {type: Array}
        };
    }

    constructor() {
        super();
        this.movies = [];
        this.getMovieData();
    }

    render() {
        return html `
            ${this.movies.map(
                movie => 
                    html`
                        <div>La pelicula ${movie.category_id}, fue dirigida por ${movie.description}</div>
                    `
            )}
        `;
    }

    getMovieData() {
        console.log("getMovieData");
        console.log("Obteniendo datos de los filmes de SW");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                this.movies = APIResponse;
            }
        }

        xhr.open("GET", "https://hackaton-g8.herokuapp.com/categoriatarjetas", false);
        xhr.send();
    }
}

customElements.define('test-api', TestApi)