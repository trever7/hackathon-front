import { LitElement, html } from 'lit-element';

class HolaMundo extends LitElement {
    render() {
        return html `
            <div>Hola mundo LitElement!</div>
        `;
    }
}

customElements.define('hola-mundo', HolaMundo)