import {LitElement, html} from 'lit-element';

/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class TarjetaNueva extends LitElement {
    static get properties() {
        return {
            nomCliente:{type:String}, 
            nroTarjeta:{type:String}, 
            fechaCaduca:{type:String},
            categoria:{type:String},
            tipoTarjeta:{type:String}, 
            tipoTarjetaTrans:{type:String},
            limiteTarjeta:{type:Number},
            saldoTarjeta:{type:Number} 
        }
    }

    /**
     * Instance of the element is created/upgraded. Use: initializing state,
     * set up event listeners, create shadow dom.
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
         <form class="row align-items-start">
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col col-md-6 col-sm-3 col-xs-12">
                <div class="d-flex justify-content-center">
                    <h3 class="card-title">Nueva Tarjeta</h3>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Nombre del Cliente</label>
                    <input type="text" id="exampleFormControlInput1" class="form-control" name="ncliente" value="${this.nomCliente}" 
                     @input="${this.updateNombCliente}"></input>
 
                </div>
                <div class="form-group">
                    <label>Número de Tarjeta</label>
                    <input type="text" id="fname" name="fname" class="form-control" value="${this.nroTarjeta}" 
                    @input="${this.updateNrotarjeta}"></input>
                </div>  
                <div class="form-group">
                    <label>Fecha de Caducidad</label>
                    <input type="date" name="fechaCaduca" value="${this.fechaCaduca}" class="form-control" 
                    @input="${this.updateFechaCaducidad}"></input>
                </div> 
                <div class="form-group">
                    <label>Categoría</label>
                    <select id="categoria" class="form-control">
                       <option> Transporte</option>
                       <option> GiftCard</option>
                       <option> Alimentación</option>
                    </select>
                </div>
                <label>Tipo de Tarjeta</label>
                <div class="form-row">
                    <div class="form-group col-md-6">
                       <select id="tipoTarjeta" class="form-control">
                           <option> Visa</option>
                           <option> Mastercard</option>
                       </select> 
                    </div>
                    <div class="form-group col-md-6">
                       <select id="tipoTarjetaTrans" class="form-control">
                           <option> Metropolitano</option>
                           <option> Metro de Lima</option>
                           <option> Corredor</option>
                       </select>
                    </div>

                </div>
                <div class="form-group" > 
                    <label>Limite S/. </label>
                    <input type="text" id="fname" name="fname" value="${this.limiteTarjeta}" class="form-control"></input>
                </div>
                <div>
                    <label>Saldo Inicial S/. </label>
                    <input type="text" name="saInicial" class="form-control" value="${this.saldoTarjeta}"></input>
                </div>
                <div class="card-footer">
                   <button @click="${this.regresa}" class="btn btn-danger  col-5"><strong>Volver</strong></button>
                   <button @click="${this.actualizacarga}" class="btn btn-info offset-1 col-5"><strong>Guardar</strong></button>
                </div>
            </div>
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
          </form>
        `;
    }

    regresa(e) {
        console.log("regresa");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("tarjeta-cerrar-form",{}));
    }

    resetFormData() {
        console.log("resetFormData en tarjeta-nueva");
        // this.person = {};
        // this.person.name = "";
        // this.person.profile = "";
        // this.person.yearsInCompany = "";

        // this.editingPerson = false;
    }

}

customElements.define('tarjeta-nueva', TarjetaNueva);